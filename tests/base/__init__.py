#
# Copyright (C) 2018 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA.
#
"""
Provide tests with some base utility.
"""

import os
import sys
import uuid
import shutil
import tempfile
from unittest import TestCase as BaseCase

# python 2.7 and python 3.5 support
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

TEST_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


class StdRedirect(object): # pylint: disable=too-few-public-methods
    """Capture printed output, or provide standard input

    with StdRedirect('stdout') as out:
        print("expected")
        str(out) == 'expected'

    with StdRedirect('stdin', 'data') as inp:
        sys.stdin.read() == 'data'
        imp += 'more data arrived'
    """
    def __init__(self, name='stdout', initial=None):
        self.name = name
        self.std = getattr(sys, self.name)
        self.str = StringIO(initial)

    def __enter__(self):
        setattr(sys, self.name, self.str)
        return self

    def __str__(self):
        self.str.seek(0)
        return self.str.read()

    def __repr__(self):
        return "<StdRedirect {}>".format(self.name)

    def __iadd__(self, data):
        self.str.seek(0, mode=2)
        self.str.write(data)
        self.str.seek(0, mode=1)

    def __exit__(self, kind, value, traceback):
        setattr(sys, self.name, self.std)

class NoExtension(object): # pylint: disable=too-few-public-methods
    """Test case must specify 'ext_model' to assertEffect."""
    def __init__(self, *args, **kwargs):
        raise NotImplementedError(self.__doc__)

    def run(self, *args, **kwargs):
        """Fake run"""
        pass

class TestCase(BaseCase):
    """
    Base class for all effects tests, provides access to data_files and test_without_parameters
    """
    ext_model = NoExtension

    def __init__(self, *args, **kw):
        super(TestCase, self).__init__(*args, **kw)
        self.temp_dir = None

    def tearDown(self):
        if self.temp_dir and os.path.isdir(self.temp_dir):
            shutil.rmtree(self.temp_dir)

    def temp_file(self, prefix='file-', template='{prefix}{name}{suffix}', suffix='.tmp'):
        """Generate the filename of a temporary file"""
        if not self.temp_dir:
            self.temp_dir = tempfile.mkdtemp(prefix='inkex-tests-')
        if not os.path.isdir(self.temp_dir):
            raise IOError("The temporary directory has disappeared!")
        filename = template.format(prefix=prefix, suffix=suffix, name=uuid.uuid4().hex)
        return os.path.join(self.temp_dir, filename)

    @staticmethod
    def data_file(filename, *parts):
        """Provide a data file from a filename, can accept directories as arguments."""
        full_path = os.path.join(TEST_ROOT, 'data', filename, *parts)
        if not os.path.isfile(full_path):
            raise IOError("Can't find test data file: {}".format(filename))
        return full_path

    @property
    def root_dir(self):
        """Return the full path to the extensions directory"""
        return os.path.abspath(os.path.join(TEST_ROOT, '..'))

    @property
    def empty_svg(self):
        """Returns a common minimal svg file"""
        return self.data_file('svg', 'default-inkscape-SVG.svg')

    def assertEffectEmpty(self, effect, **kwargs): # pylint: disable=invalid-name
        """Assert calling effect without any arguments"""
        self.assertEffect(effect=effect, **kwargs)

    def assertEffect(self, *filename, **kwargs): # pylint: disable=invalid-name
        """Assert an effect, capturing the output to stdout.

           filename should point to a starting svg document, default is empty_svg
        """
        contains = kwargs.pop('contains', None)
        effect = kwargs.pop('effect', self.ext_model)()

        args = [self.data_file(*filename)] if filename else [self.empty_svg] # pylint: disable=no-value-for-parameter
        args += kwargs.pop('args', [])
        args += ['--{}={}'.format(*kw) for kw in kwargs.items()]

        with StdRedirect() as out:
            effect.run(args)
            if contains is not None:
                self.assertIn(contains, out)
            str(out)

        if os.environ.get('FAIL_ON_DEPRICATION', False):
            warnings = getattr(effect, 'warned_about', set())
            effect.warned_about = set() # reset for next test
            self.assertFalse(warnings, "Deprecated API is still being used!")

        return effect
